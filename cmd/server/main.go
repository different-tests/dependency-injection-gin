package main

import (
	
	"context"
	"fmt"
	"dependency-injection-gin/cmd/server/wire"
	
)

func main() {
	ctx := context.Background()
	server, err := wire.InitializeServer(ctx)
	if err != nil {
		fmt.Println("Failed to initialize Server:", err)
		return
	}

	server.StartCron()
	server.SetupRoutes()
	server.Router.Run(":8080")
}