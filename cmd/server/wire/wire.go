//go:build wireinject
// +build wireinject

package wire

import (
	"context"

	"log"
	"net/http"

	"github.com/gin-gonic/gin"

	"github.com/google/wire"

	"dependency-injection-gin/internal/server"
)


type Message string

func NewMessage() Message {
	return Message("Hello, World!")
}

type Croner struct {
	Message Message
}

func NewCroner(message Message) Croner {
	return Croner{
		Message: message,
	}
}

func (g Croner) Greet() string {
	return string(g.Message)
}

type Greeter struct {
	Message Message
}

func NewGreeter(message Message) Greeter {
	return Greeter{
		Message: message,
	}
}

func (g Greeter) Greet() string {
	return string(g.Message)
}

type Server struct {
	Router  *gin.Engine
	Greeter Greeter
	Croner  Croner

}

func NewServer(router *gin.Engine, greeter Greeter, croner Croner) Server {
	return Server{
		Router:  router,
		Greeter: greeter,
		Croner:  croner,
		
	}
}
func (s *Server) StartCron() {
	log.Println("STARR CRON")

	ncr := server.NewCron()
	ncr.Start(context.Background())

}

func (s *Server) SetupRoutes() {
	s.Router.GET("/", s.handleHome)
}

func (s *Server) handleHome(c *gin.Context) {
	message := s.Greeter.Greet()
	c.String(http.StatusOK, message)
}

func InitializeServer(ctx context.Context) (Server, error) {
	wire.Build(NewMessage, NewGreeter, NewCroner, NewServer, gin.Default)
	return Server{}, nil
}
