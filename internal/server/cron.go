package server

import (
	"context"
	"log"
	"time"

	"github.com/go-co-op/gocron"
)
func printHello() {
	

	log.Println("Hello, World!")
}
type Cron struct {
}

func NewCron() *Cron {
	return &Cron{}
}
func (j *Cron) Start(ctx context.Context) error {
	// eg: kafka consumer

	cr := gocron.NewScheduler(time.Local)

	// cr.Cron("*/1 * * * * *").Do(printHello)

	cr.WaitForScheduleAll()
	cr.Every(2).Second().Do(printHello)


	cr.StartAsync()

	return nil
}
func (j *Cron) Stop(ctx context.Context) error {
	return nil
}
